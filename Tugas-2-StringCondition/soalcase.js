var tanggal=1; 
var bulan=2; 
var tahun=2000; 
var bulantampil='';
if ( (tanggal=='' || isNaN(tanggal)) || (bulan=='' || isNaN(bulan)) || (tahun=='' || isNaN(tahun)) ) {
    console.log('Lengkapi datanya dulu ya');
} else {
    if (tanggal<1 || tanggal >31) {
        console.log('Tanggal tidak valid');
    } else { 
        if (bulan<1 || bulan >12) {
            console.log('Bulan tidak valid')
        } else {
            if (tahun <1900 || tahun >2200) {
                console.log('tahun tidak valid')
            }else{
                switch (Number(bulan)) {
                    case 1:   { bulantampil='Januari'; break; }
                    case 2:   { bulantampil='Februari'; break; }
                    case 3:   { bulantampil='Maret'; break; }
                    case 4:   { bulantampil='April'; break; }
                    case 5:   { bulantampil='Mei'; break; }
                    case 6:   { bulantampil='Juni'; break; }
                    case 7:   { bulantampil='Juli'; break; }
                    case 8:   { bulantampil='Agustus'; break; }
                    case 9:   { bulantampil='September'; break; }
                    case 10:   { bulantampil='Oktober'; break; }
                    case 11:   { bulantampil='November'; break; }
                    case 12:   { bulantampil='Desember'; break; }
                }
                console.log(`${tanggal} ${bulantampil} ${tahun}`);
            }
        }
    }
}
