var nama = 'jeni';
var peran = 'penyihir';

if (nama=='' && peran=='') {
  console.log('Nama Harus diisi');
} else if (nama!='' && peran=='') {
  console.log(`Hai ${nama}!, Pilih peranmu untuk memulai game!`);
} else if (nama!='' && peran!='') {
  console.log(`Selamat datang di Dunia Werewolf, ${nama}!`);
  if (peran.toUpperCase()=='PENYIHIR') {
    console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
  } else if (peran.toUpperCase()=='GUARD') {
    console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`);
  } else if (peran.toUpperCase()=='WAREWOLF') {
    console.log(`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`);
  } else {
    console.log(`Halo ${peran} ${nama}, Pilihan peranmu tidak sesuai, pilih Penyihir, Guard, atau Werewolf ya !`);
  }
}