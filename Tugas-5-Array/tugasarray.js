function range(no1, no2) {
    var hasil = [];
    for (var satu = no1; satu <= no2; satu++) {
        if (no1 == '' && no2 == '') {
            hasil.push('-1');
        } else {
            hasil.push(satu);
        }
    }
    return hasil;
}
console.log('Hasil 1');
console.log(range(1, 10));

function rangeWithStep(startNum, finishNum, step) {
    var hasil2 = [];
    while (startNum <= finishNum) {
        hasil2.push(startNum);
        startNum += 2;

    }
    return hasil2;
}
console.log('\nHasil 2');
console.log(rangeWithStep(1, 10, 2));

function sumindex(startNum, finishNum, step = '') {
    var hasil3;
    var hasilsum = 0;
    if (step == '') {
        hasil3 = range(startNum, finishNum);
    } else {
        hasil3 = rangeWithStep(startNum, finishNum, step)
    }
    hasil3.forEach(function (item) {
        var itemx = parseInt(item)
        hasilsum += itemx;
    });
    return hasilsum;
}
console.log('\nHasil 3');
console.log(sumindex(1, 10));
console.log(sumindex(5, 50, 2));
console.log('\nHasil 4');

function dataHandling() {
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ];
    input.forEach(function (ind) {
        console.log("Nomor ID: " + ind[0]);
        console.log("Nama Lengkap: " + ind[1]);
        console.log("TTL: " + ind[2] + " " + ind[3]);
        console.log("Hoby: " + ind[4] + '\n');
    })
}
dataHandling();

function balikKata(kata) {
    var hsl = '';
    for (var index = kata.length; index >= 0; index--) {
        hsl += kata.substr(index, 1);
    }
    return (hsl);
}
console.log('Hasil 5');

console.log(balikKata("Kasur Rusak"));

console.log('\nHasil 6');

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2();

function dataHandling2() {
    // var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
    var hasilHandling;
    hasilHandling = input.splice(1, 1, 'Roman Alamsyah Elsharawy');
    hasilHandling = input.splice(2, 1, 'Provinsi Bandar Lampung');
    hasilHandling = input.splice(4, 2, "Pria", "SMA Internasional Metro");
    var tgl = input[3];
    var bulan = tgl.substr(3, 2);
    switch (Number(bulan)) {
        case 1: { bulantampil = 'Januari'; break; }
        case 2: { bulantampil = 'Februari'; break; }
        case 3: { bulantampil = 'Maret'; break; }
        case 4: { bulantampil = 'April'; break; }
        case 5: { bulantampil = 'Mei'; break; }
        case 6: { bulantampil = 'Juni'; break; }
        case 7: { bulantampil = 'Juli'; break; }
        case 8: { bulantampil = 'Agustus'; break; }
        case 9: { bulantampil = 'September'; break; }
        case 10: { bulantampil = 'Oktober'; break; }
        case 11: { bulantampil = 'November'; break; }
        case 12: { bulantampil = 'Desember'; break; }
    }
    tgl = tgl.split("/")
    console.log(input);
    console.log(bulantampil);
    console.log(tgl.sort(function (a, b) { return b - a }));
    console.log(input[3]);
    var namax = input[1];
    var nama = namax.slice(0, 15);
    console.log(nama);
}