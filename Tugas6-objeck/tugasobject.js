function arrayToObject(arr) {
    var now = new Date();
    var thisYear = now.getFullYear();
    var umur;
    for (var index = 0; index < arr.length; index++) {
        umur = thisYear - arr[index][3];
        if (umur <= 0 || isNaN(umur)) {
            umur = 'Invalid Birth Year';
        }
        var ident = {
            firstName: arr[index][0],
            lastName: arr[index][1],
            gender: arr[index][2],
            age: umur
        }
        console.log(index + 1 + '. ' + ident.firstName + ' ' + ident.lastName + ' ' + JSON.stringify(ident));
    }
}
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people);
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2);


// Tugas 2
console.log('\nTugas 2');
function shoppingTime(memberId, money) {

    if (!memberId) {
        console.log('Mohon maaf, toko X hanya berlaku untuk member saja');
        return;
    }
    if (money < 50000) {
        console.log('Mohon maaf, uang tidak cukup');
        return;
    }
    var sisa = money;
    var produk = [];
    var cek1 = 0, cek2 = 0, cek3 = 0, cek4 = 0, cek5 = 0
    while (sisa >= 50000 && cek5 == 0) {
        if (sisa >= 1500000 && cek1 == 0) {
            produk.push('Sepatu brand Stacattu');
            sisa = sisa - 1500000;
            cek1 = 1;
        } else if (sisa >= 500000 && cek2 == 0) {
            produk.push('Baju brand Zoro');
            sisa = sisa - 500000;
            cek2 = 1;
        } else if (sisa >= 250000 && cek3 == 0) {
            produk.push('Baju brand H & N');
            sisa = sisa - 250000;
            cek3 = 1;
        } else if (sisa >= 175000 && cek4 == 0) {
            produk.push('Sweater brand Uniklooh');
            sisa = sisa - 175000;
            cek4 = 1;
        } else if (sisa >= 50000 && cek5 == 0) {
            produk.push('Casing Handphone');
            sisa = sisa - 50000;
            cek5 = 1;
        }
    }
    hasil = {
        memberId: memberId,
        money: money,
        listPurchased:
            produk,
        changeMoney: sisa
    }
    return hasil;
}
shoppingTime('', 50000)
shoppingTime('234JdhweRxa53', 15000);
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));

// Tugas 3
console.log('\nTugas 3');
function naikAngkot(arrPenumpang) {

    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    output = []
    if (arrPenumpang.length <= 0) {
        return [];
    }
    
    var jmlRute;
    var bayar;
    for (var i = 0; i < arrPenumpang.length; i++) {
        var asal = arrPenumpang[i][1];
        var tujuan = arrPenumpang[i][2];
        var outObj = {};
        for (var arah = 0; arah < rute.length; arah++) {
            if (asal==rute[arah]) {
                jmlRute = arah;
            }else if (tujuan==rute[arah]) {
                jmlRute=arah-jmlRute;
            }
        }
        bayar=jmlRute*2000;
        outObj.penumpang = arrPenumpang[i][0]
        outObj.naikDari = asal
        outObj.tujuan = tujuan
        outObj.bayar = bayar
        output.push(outObj)
    }

    return output;
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));