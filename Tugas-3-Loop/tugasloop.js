var angka=2;
console.log('LOOPING PERTAMA');
while(angka<21){
    console.log(angka + " - I Love Coding");
    angka+=2;

}
console.log('LOOPING KEDUA');
while(angka>2){
    angka-=2;
    console.log(angka + " - I Love Coding");  
}
console.log('');
console.log('LOOPING KETIGA FOR');
for (var ulang2 = 1; ulang2 <=20; ulang2++) {
    if((ulang2%3)===0 && ((ulang2%2)==1)){
        console.log(ulang2 + " - I Love Coding");
    } else if ((ulang2%2)==1) { //ganjil
        console.log(ulang2 + " - Santai");
    } else {
        console.log(ulang2 + " - Berkualitas");
    }    
}
console.log('');
console.log('Membuat Persegi Panjang');
var tampil='';
for (var ulang3 = 1; ulang3 <=4; ulang3++) {    
    for (var ulang3a = 1; ulang3a <=8; ulang3a++) {
        tampil+="#";        
    }   
    console.log(tampil);
    tampil='';
}
console.log('');
console.log('Membuat Tangga');
var tangga='';
for (var ulang4 = 1; ulang4 <= 7; ulang4++) {    
    for (var ulang4a = 1; ulang4a <= ulang4; ulang4a++) {
        tangga+="#";        
    }   
    console.log(tangga);
    tangga='';
}
console.log('');
console.log('Membuat Papan Catur');
var catur='';
for (var ulang5 = 1; ulang5 <= 8; ulang5++) {    
    for (var ulang5a = 1; ulang5a <= 8; ulang5a++) {
        if ((ulang5+ulang5a)%2 === 1) {
            catur+='#';
        } else {
            catur+=' ';
        }               
    }   
    console.log(catur);
    catur='';
}