function readBookPromise(time, book) {
    console.log(`saya membaca ${book.name}`);
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            let sisawaktu = time - book.timeSpent
            if (sisawaktu >= 0) {
                console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisawaktu}`);
                resolve(sisawaktu)
            } else {
                console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`);
                let err = new Error('Waktu habis!');
                reject(err)
            }
        }, book.timeSpent)
    })

}

module.exports = readBookPromise