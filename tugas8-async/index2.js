let readBooksPromise = require('./promise.js')

let books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]
let index=0
function baca2(time) {
    readBooksPromise(time,books[index])
        .then(function (timeremain) {            
            if (timeremain != time && index < books.length - 1) {
                index++
                baca2(timeremain)
            }
        })
        .catch(function (error) {
            console.log(`\n${error.message}`);
        });
}

baca2(5000)