let readBooks = require('./callback.js')

let books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]
let index = 0;
function baca(time) {
    readBooks(time, books[index], function (timeremain) {
        if (timeremain != time && index < books.length-1) {
            index++;
            baca(timeremain, books[index])
        }
    })
}
baca(10000);