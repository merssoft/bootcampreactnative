//soal ke 1
function teriak() {
    var tampil = "Halo Sanbers!";
    return tampil;
}
var tampung1 = teriak();
console.log(tampung1);

//soal ke 2
function kalikan(angka1, angka2) {
    var hasil = angka1 * angka2;
    return hasil;
}
var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali)


//soal ke 3
function introduce(nama, umur, alamat, hobi) {
    var kenal = "Nama saya " + nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobi
    return kenal;
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)