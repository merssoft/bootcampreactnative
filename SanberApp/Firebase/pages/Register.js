import React, { useState, useEffect } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import * as firebase from 'firebase';

export default function Register({ navigation }) {
    const firebaseConfig = {
        apiKey: "AIzaSyDSdMA_5F1_t_1zmcrVuNsCP2Ak6B0zlT0",
        authDomain: "authenticationfirebasern-2b7ba.firebaseapp.com",
        databaseURL: "https://authenticationfirebasern-2b7ba-default-rtdb.firebaseio.com",
        projectId: "authenticationfirebasern-2b7ba",
        storageBucket: "authenticationfirebasern-2b7ba.appspot.com",
        messagingSenderId: "886958605342",
        appId: "1:886958605342:web:b4ddbdb498224cf8601a0f"
    };
    // // Initialize Firebase
    // firebase.initializeApp(firebaseConfig);
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig)
    }
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const submit = () => {
        const data = {
            email,
            password
        }
        console.log(data)
        firebase.auth().createUserWithEmailAndPassword(email, password).then(() => {
            console.log('Register Berhasil');
            navigation.navigate("Home");
        }).catch(() => {
            console.log("register gagal")
        })
    }
    return (
        <View style={styles.container}>
            <Text>Register</Text>
            <TextInput
                style={styles.input}
                placeholder="Masukin Email"
                value={email}
                onChangeText={(value) => setEmail(value)}

            />
            <TextInput
                style={styles.input}
                placeholder="Masukin Password"
                value={password}
                onChangeText={(value) => setPassword(value)}
            />
            <Button onPress={submit} title="REGISTER" />
            <TouchableOpacity style={{ marginTop: 30 }}
                // onPress={()=>navigation.navigate("Register")}
                onPress={() => alert("hello world")}
            >
                <Text>Buat Akun</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        borderWidth: 1,
        borderColor: 'grey',
        paddingHorizontal: 10,
        paddingVertical: 10,
        width: 300,
        marginBottom: 10,
        borderRadius: 6,
        marginTop: 10
    }
})
