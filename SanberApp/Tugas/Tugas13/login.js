import React from 'react'
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'

// const login = () => {
export default function Login() {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.subHeader}>
                    <Image
                        // style={styles.logo}                    
                        source={require('./asset/logo.png')}
                    />
                </View>
            </View>
            <View style={styles.judul}>
                <Text style={{ fontSize: 24 }}>Login</Text>
            </View>

            <View style={styles.body}>

                <View style={styles.subbody}>

                    <Text style={styles.jarak}>UserName</Text>
                    <View >
                        <TextInput style={styles.textbox}
                            placeholder="Input your username here"
                        ></TextInput>
                    </View>
                    <Text style={styles.jarak}>Password</Text>
                    <View >
                        <TextInput style={styles.textbox}
                            placeholder="Input your password here"
                        ></TextInput>
                    </View>
                    <View style={{ alignItems: 'center', width: 310 }}>
                        <TouchableOpacity>
                            <View style={styles.tombol}
                                backgroundColor='#5393F3'>
                                <Text>Login</Text>
                            </View>
                        </TouchableOpacity>
                        <Text style={styles.jarak}>
                            Atau</Text>
                        <TouchableOpacity>
                            <View style={styles.tombol}
                                backgroundColor='white'>
                                <Text>Register</Text>
                            </View>
                        </TouchableOpacity>

                    </View>


                </View>

            </View>
            <View style={styles.body2}>
                <Text style={{ fontSize: 20, color: 'white', fontStyle: 'italic' }}>IDEA We Share We Care</Text>
            </View>

        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        height: 100,
        backgroundColor: 'white',
        justifyContent: 'center',
        marginTop: 60,
    },
    subHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        justifyContent: 'center'
    },
    judul: {
        flexDirection: 'row',
        marginTop: 30,
        marginHorizontal: 50,
        alignItems: 'flex-start',

    },
    body: {
        flexDirection: 'row',
        marginTop: 10,
        marginHorizontal: 50,
        alignItems: 'flex-start',
        // justifyContent: 'center',
        backgroundColor: '#F2994A',
        height: 357,
        borderRadius: 10
        // marginLeft: 50,
        // margin: 50,

    },
    subbody: {
        // marginTop: 50,
        // height: 50,
        margin: 20,
        // flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        // backgroundColor: 'white',
        height: 337,
        // margin:5
        // width: 303,
        // marginLeft: 50,

    },
    body2: {
        flexDirection: 'row',
        marginTop: 10,
        marginHorizontal: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F2994A',
        height: 70,
        borderRadius: 10
        // marginLeft: 50,
        // margin: 50,

    },
    textbox: {
        width: 310, height: 40, borderColor: 'gray', borderWidth: 1,
        backgroundColor: 'white'
    },
    tombol: {
        height: 40,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 10,
        borderRadius: 5,
        fontSize: 16
    },
    jarak: {
        marginTop: 10,
        fontSize: 16,
        color: 'black'
    }

})