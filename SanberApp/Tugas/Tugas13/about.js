import React from 'react'
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'

// const login = () => {
export default function About() {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.judul}>
                    <Text style={{ fontSize: 34 }}>Tentang Saya</Text>
                </View>
            </View>


            <View style={styles.body}>
                <View style={styles.logo}                    >
                    <Image
                        style={{borderRadius:10}}                    
                        source={require('./asset/logo.png')}
                    />
                </View>
                <Text style={styles.jarak}>Daniel Fernandes</Text>
                <Text style={styles.jarak}>React Native Developer</Text>
                <View style={styles.subbody}
                    alignItems='center'>

                    <Text style={styles.jarak2}>Portofolio :</Text>
                    <View style={styles.portofolio}

                    >
                        <Image
                            style={styles.icon}
                            source={require('./asset/php2.png')}
                        />
                        <Text>    </Text>
                        <Image
                            style={styles.icon}
                            source={require('./asset/ci.png')}
                        />
                        <Text>    </Text>
                        <Image
                            style={styles.icon}
                            source={require('./asset/android.png')}
                        />
                    </View>
                </View>
                <View style={styles.subbody}
                    // justifyContent='center'
                    alignItems='center'
                >
                    <Text style={styles.jarak2}>Contact :</Text>

                    <Image
                        style={styles.jarak}
                        source={require('./asset/contact.png')}
                    />
                </View>

            </View>


        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        height: 100,
        // backgroundColor: '#F2994A',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
    },
    logo: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        marginTop: 30,
        justifyContent: 'center'
    },
    judul: {
        flexDirection: 'row',
        // marginTop: 30,
        marginHorizontal: 50,
        alignItems: 'flex-start',

    },
    body: {
        // flexDirection: 'row',
        marginTop: 10,
        marginHorizontal: 50,
        alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: '#F2F2F2',
        height: 620,
        // marginBottom:50,
        borderRadius: 10
        // marginLeft: 50,
        // margin: 50,

    },
    subbody: {
        marginTop: 30,
        // height: 50,
        flexDirection: 'column',
        // margin: 20,
        // flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        // paddingLeft: 20,
        // marginHorizontal:50,
        height: 150,
        // margin:5
        width: 310,
        // marginLeft: 50,

    },
    body2: {
        flexDirection: 'row',
        marginTop: 10,
        marginHorizontal: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F2994A',
        height: 60,
        borderRadius: 10
        // marginLeft: 50,
        // margin: 50,

    },
    textbox: {
        width: 310, height: 40, borderColor: 'gray', borderWidth: 1,
        backgroundColor: 'white'
    },
    tombol: {
        height: 40,
        width: 162,
        alignItems: 'center',
        justifyContent: 'center',
        // marginTop: 20,
        // marginBottom:10,
        borderRadius: 5,
        fontSize: 16
    },
    jarak: {
        marginTop: 30,
        fontSize: 16,
        color: 'black'
    },
    jarak2: {
        marginTop: 10,
        fontSize: 16,
        textDecorationLine: 'underline',
        color: 'black'
    },
    icon: {
        marginTop: 20,
        marginVertical: 20,
        padding:30
        // height: 70,
        // width: 50
    },
    portofolio: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'

    }

})