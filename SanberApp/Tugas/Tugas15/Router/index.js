// import 'react-native-gesture-handler';
import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


import AboutScreen from '../pages/AboutScreen';
import LoginScreen from '../pages/Login';
import AddScreen from '../pages/AddScreen';
import HomeScreen from '../pages/Home';
import ProjectScreen from '../pages/ProjectScreen';
import Setting from '../pages/Setting';
import SkillProject from '../pages/SkillProject';
import Home from '../pages/Home';

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();
export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="LoginScreen" component={LoginScreen} />
                <Stack.Screen name="HomeScreen" component={HomeScreen} />
                <Stack.Screen name="MainApp" component={MainApp} />
                <Stack.Screen name="MyDrawwer" component={MyDrawwer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () => (
    <Tab.Navigator>
        <Tab.Screen name="Aboutscreen" component={AboutScreen} />
        <Tab.Screen name="AddScreen" component={AddScreen} />
        <Tab.Screen name="SkillProject" component={SkillProject} />
    </Tab.Navigator>
)


const MyDrawwer = () => (
    <Drawwer.Navigator>
        <Drawwer.Screen name="App" component={MainApp} />
        <Drawwer.Screen name="AboutScreen" component={AboutScreen} />
    </Drawwer.Navigator>
)

