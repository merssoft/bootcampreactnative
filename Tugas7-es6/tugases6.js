// const golden = function goldenFunction() {
//     console.log("this is golden!!")
// }
let golden = () => {
    console.log("this is golden!!")
}
golden()

const newFunction = (firstName, lastName) => {
    return {
        fullName: () => console.log(`${firstName} ${lastName}`)
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
let {firstName, lastName, destination, occupation}=newObject;
console.log(firstName, lastName, destination, occupation);

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
console.log(combined)

const planet = "earth"
const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit,${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
console.log(before) 
